package root.appevu03.rest;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import root.appevu03.dao.AlbumJpaController;
import root.appevu03.dao.exceptions.NonexistentEntityException;
import root.appevu03.entity.Album;

@Path("album")
public class AlbumRest {

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response listaAlbums() {
        AlbumJpaController dao = new AlbumJpaController();
        List<Album> listaAlbum = dao.findAlbumEntities();
        return Response.ok(200).entity(listaAlbum).build();
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response crear(Album album) {
        AlbumJpaController dao = new AlbumJpaController();
        try {
            dao.create(album);
        } catch (Exception ex) {
            Logger.getLogger(AlbumRest.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Response.ok(200).entity(album).build();
    }
    
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public Response editar(Album album) {
      AlbumJpaController dao = new AlbumJpaController();  
        try {
            dao.edit(album);
        } catch (Exception ex) {
            Logger.getLogger(AlbumRest.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return Response.ok(200).entity(album).build();
    }
    
    
    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{id}")
    public Response eliminar(@PathParam("id") String id) {
        AlbumJpaController dao = new AlbumJpaController();  
        try {
            dao.destroy(id);
        } catch (NonexistentEntityException ex) {
            Logger.getLogger(AlbumRest.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return Response.ok("album eliminado").build();   
    }

}
